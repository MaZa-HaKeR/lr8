#include "Header.h"
#define _USE_MATH_DEFINES
#include <cmath>

float first(int x, int y) {
	return (1 + pow(sin(x), 2)) / (2 + abs(y - 3 * y / (1 + pow(x, 3) * pow(y, 2)))) + sqrt(x) / (pow(x, 1. / 3));
}
float second(int z, int x, int y) {
	return pow(cos(tan(x + y)), 2) + sin(z - (270 * M_PI / 180));
}
float chamto(float b)
{
	return static_cast<int>((b * 1000)) % 10 + static_cast<int>((b * 100)) % 10 + static_cast<int>((b * 10)) % 10;
}
